package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.Client;
import org.cloudburstmc.math.vector.Vector3i;
import com.github.steveice10.packetlib.packet.Packet;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.SessionListener;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.ServerboundCommandSuggestionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.clientbound.ClientboundCommandSuggestionsPacket;
import java.util.concurrent.CompletableFuture;
import java.util.Map;
import java.util.HashMap;

public class TabCompletePlugin extends SessionAdapter {
  private Client client;
  private int nextTransactionId = 0;
  private Map<Integer, CompletableFuture<ClientboundCommandSuggestionsPacket>> transactions = new HashMap<>();

  public TabCompletePlugin (Client client) {
    this.client = client;
    client.addListener((SessionListener) this);
  }

  public CompletableFuture<ClientboundCommandSuggestionsPacket> complete (String command) {
    final int transactionId = nextTransactionId++;
    if (nextTransactionId > Integer.MAX_VALUE) nextTransactionId = 0; // ? Can and should I use negative numbers too?
    client.session().send(new ServerboundCommandSuggestionPacket(transactionId, command));

    final CompletableFuture<ClientboundCommandSuggestionsPacket> future = new CompletableFuture<ClientboundCommandSuggestionsPacket>();
    transactions.put(transactionId, future);
    return future;
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundCommandSuggestionsPacket) packetReceived(session, (ClientboundCommandSuggestionsPacket) packet);
  }

  public void packetReceived (Session session, ClientboundCommandSuggestionsPacket packet) {
    final CompletableFuture<ClientboundCommandSuggestionsPacket> future = transactions.get(packet.getTransactionId());
    if (future == null) return;
    future.complete(packet);
    transactions.remove(future);
  }
}
