package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.NamedTextColor;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.ArrayList;

public class CommandSpyPlugin extends ChatPlugin.Listener {
  private final ChipmunkBot client;
  @Getter private List<Listener> listeners = new ArrayList<>();
  @Getter @Setter private boolean enabled = false;

  private static final Style ENABLED_STYLE = Style.style(NamedTextColor.YELLOW);
  private static final Style DISABLED_STYLE = Style.style(NamedTextColor.AQUA);

  private static final Component COMMANDSPY_ENABLED_COMPONENT = Component.text("Successfully enabled CommandSpy");
  private static final Component COMMANDSPY_DISABLED_COMPONENT = Component.text("Successfully disabled CommandSpy");
  private static final Component COMMAND_SEPARATOR_COMPONENT = Component.text(": ");
  private static final Component SIGN_CREATED_TEXT_COMPONENT = Component.text(" created a sign with contents:");
  private static final Component SIGN_LINE_SEPARATOR_COMPONENT = Component.text("\n ");

  public CommandSpyPlugin (ChipmunkBot client) {
    this.client = client;
    client.chat().addListener((ChatPlugin.Listener) this);
  }

  @Override
  public void systemMessageReceived (Component component, boolean overlay) {
    if (overlay || !(component instanceof final TextComponent t_component)) return;

    if (component.equals(COMMANDSPY_ENABLED_COMPONENT)) { this.enabled = true; return; }
    if (component.equals(COMMANDSPY_DISABLED_COMPONENT)) { this.enabled = false; return; }

    final boolean enabled = component.style().equals(ENABLED_STYLE);
    if (!enabled && !component.style().equals(DISABLED_STYLE)) return;

    final String username = t_component.content();
    final MutablePlayerListEntry sender = client.playerList().getEntry(username);
    if (sender == null) return;

    final List<Component> children = component.children();

    if (children.size() == 2) {
      // Command
      final Component separator = children.get(0);
      final Component prefixedCommand = children.get(1);
      if (
        !(separator instanceof final TextComponent t_separator) ||
        !(prefixedCommand instanceof final TextComponent t_prefixedCommand) ||
        !separator.equals(COMMAND_SEPARATOR_COMPONENT) ||
        !prefixedCommand.style().isEmpty()
      ) return;

      final String command = t_prefixedCommand.content();
      if (command.length() < 1 || command.charAt(0) != '/') return;
      final String rawCommand = command.substring(1);

      for (Listener listener : listeners) listener.commandReceived(sender, rawCommand, enabled);
    } else if (children.size() == 9) {
      // Sign created
      final Component[] lines = new Component[4];

      if (!children.get(0).equals(SIGN_CREATED_TEXT_COMPONENT)) return;

      for (int i = 0; i < lines.length; i++) {
        int separatorChildIndex = (i * 2) + 1;
        if (!children.get(separatorChildIndex).equals(SIGN_LINE_SEPARATOR_COMPONENT)) return;
        lines[i] = children.get(separatorChildIndex + 1);
      }

      for (Listener listener : listeners) listener.signCreated(sender, lines, enabled);
    }
  }

  public static class Listener {
    public void commandReceived (MutablePlayerListEntry sender, String command, boolean senderHasCommandSpy) {}
    public void signCreated (MutablePlayerListEntry sender, Component[] lines, boolean senderHasCommandSpy) {}
  }

  public void addListener (Listener listener) { listeners.add(listener); }
  public void removeListener (Listener listener) { listeners.remove(listener); }
}
