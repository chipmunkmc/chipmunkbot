package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.Options;
import land.chipmunk.chipmunkbot.plugins.PositionManager;
import land.chipmunk.chipmunkbot.data.BlockArea;
import org.cloudburstmc.math.vector.Vector3i;
import com.github.steveice10.packetlib.packet.Packet;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.SessionListener;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.event.session.DisconnectedEvent;
import com.github.steveice10.mc.protocol.packet.ingame.clientbound.entity.player.ClientboundPlayerPositionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.player.ServerboundUseItemOnPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.inventory.ServerboundSetCreativeModeSlotPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.player.ServerboundPlayerActionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.inventory.ServerboundSetCommandBlockPacket;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.level.ServerboundBlockEntityTagQuery;
import com.github.steveice10.mc.protocol.data.game.entity.metadata.ItemStack;
import com.github.steveice10.mc.protocol.data.game.entity.player.Hand;
import com.github.steveice10.mc.protocol.data.game.entity.object.Direction;
import com.github.steveice10.mc.protocol.data.game.entity.player.PlayerAction;
import com.github.steveice10.mc.protocol.data.game.level.block.CommandBlockMode;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;
import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.google.gson.JsonObject;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;

public class CommandCore extends SessionAdapter {
  private ChipmunkBot client;
  @Getter @Setter private boolean enabled = false;
  @Getter @Setter private boolean ready = false; 
  @Getter @Setter private Vector3i origin;
  // TODO: Make it configurable
  @Getter private final BlockArea relativeArea = new BlockArea(Vector3i.from(0, 0, 0), Vector3i.from(15, 0, 15));
  @Getter @Setter private Vector3i currentBlockRelative;
  private Timer refillTimer;

  @Getter private List<Listener> listeners = new ArrayList<>();

  public CommandCore (ChipmunkBot client, Options options) {
    this.client = client;
    this.enabled = options.core.enabled;
    client.addListener((SessionListener) this);
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundPlayerPositionPacket) packetReceived(session, (ClientboundPlayerPositionPacket) packet);
  }

  public void packetReceived (Session session, ClientboundPlayerPositionPacket packet) {
    if (!ready) {
      ready = true;

      if (enabled) {
        final TimerTask refillTask = new TimerTask () { @Override public void run () { refill(); } };
        refillTimer = new Timer();
        refillTimer.schedule(refillTask, 60L * 1000L, 60L * 1000L);
      }

      for (Listener listener : listeners) listener.ready();
    }

    if (!enabled) return;

    final Vector3i oldOrigin = origin;
    origin = Vector3i.from(
      ((int) packet.getX() / 16) * 16,
      0, // TODO: Use the actual bottom of the world instead of hardcoding to 0
      ((int) packet.getZ() / 16) * 16
    );

    if (currentBlockRelative == null) currentBlockRelative = Vector3i.from(relativeArea.start());
    if (!origin.equals(oldOrigin)) refill();
  }

  public void refill () {
    // final PositionManager position = client.position();
    final Vector3i relStart = relativeArea.start();
    final Vector3i relEnd = relativeArea.end();

    final String command = String.format(
      "fill %s %s %s %s %s %s minecraft:command_block",
      relStart.getX() + origin.getX(),
      relStart.getY() + origin.getY(),
      relStart.getZ() + origin.getZ(),

      relEnd.getX() + origin.getX(),
      relEnd.getY() + origin.getY(),
      relEnd.getZ() + origin.getZ()
    );

    CompoundTag itemTag = new CompoundTag("");
    CompoundTag blockEntityTag = new CompoundTag("BlockEntityTag");
    blockEntityTag.put(new StringTag("Command", command));
    blockEntityTag.put(new ByteTag("auto", (byte) 1));
    itemTag.put(blockEntityTag);

    final PositionManager position = client.position();

    Vector3i temporaryBlockPosition = Vector3i.from((int) position.x(), (int) position.y() - 1, (int) position.z());

    // client.chat().command(command);

    final Session session = client.session();
    session.send(new ServerboundSetCreativeModeSlotPacket(45, new ItemStack(373 /* command_block */, 1, itemTag)));
    session.send(new ServerboundPlayerActionPacket(PlayerAction.START_DIGGING, temporaryBlockPosition, Direction.NORTH, 0));
    session.send(new ServerboundUseItemOnPacket(temporaryBlockPosition, Direction.NORTH, Hand.OFF_HAND, 0.5f, 0.5f, 0.5f, false, 0));
  }

  public int maxCommandLength () {
    if (!enabled) return 256;
    return 32767;
  }

  public void incrementCurrentBlock () {
    final Vector3i start = relativeArea.start();
    final Vector3i end = relativeArea.end();

    int x = currentBlockRelative.getX();
    int y = currentBlockRelative.getY();
    int z = currentBlockRelative.getZ();

    x++;

    if (x > end.getX()) {
      x = start.getX();
      z++;
    }

    if (z > end.getZ()) {
      z = start.getZ();
      y++;
    }

    if (y > end.getY()) {
      x = start.getX();
      y = start.getY();
      z = start.getZ();
    }

    currentBlockRelative = Vector3i.from(x, y, z);
  }

  public Vector3i currentBlockAbsolute () {
    return currentBlockRelative.add(origin);
  }

  public void run (String command) {
    if (command.length() > maxCommandLength()) return;

    if (!enabled) {
      client.chat().command(command); // fall back to chat
      return;
    }

    final Session session = client.session();
    final Vector3i currentBlock = currentBlockAbsolute();

    // TODO: Support using repeating command blocks (on kaboom-like servers) (because less packets)
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, "", CommandBlockMode.REDSTONE, false, false, false));
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, command, CommandBlockMode.REDSTONE, false, false, true));

    incrementCurrentBlock();
  }

  public CompletableFuture<CompoundTag> runTracked (String command) {
    if (command.length() > maxCommandLength()) return emptyCompoundTagFuture();

    if (!enabled) {
      client.chat().command(command); // fall back to chat

      return emptyCompoundTagFuture();
    }

    final Session session = client.session();
    final Vector3i currentBlock = currentBlockAbsolute();

    // TODO: Support using repeating command blocks (on kaboom-like servers) (because less packets)
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, "", CommandBlockMode.SEQUENCE, false, false, false));
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, command, CommandBlockMode.REDSTONE, true, false, true));

    incrementCurrentBlock();

    CompletableFuture<CompoundTag> future = new CompletableFuture<CompoundTag>();

    final Timer timer = new Timer();
    final TimerTask queryTask = new TimerTask() {
      public void run () {
        client.query().block(currentBlock)
          .thenApply(tag -> { future.complete(tag); return tag; });

        timer.purge();
      }
    };

    timer.schedule(queryTask, 50);

    return future;
  }

  private CompletableFuture<CompoundTag> emptyCompoundTagFuture () {
    CompletableFuture<CompoundTag> future = new CompletableFuture<CompoundTag>();
    future.complete(new CompoundTag(""));
    return future;
  }

  @Override
  public void disconnected (DisconnectedEvent event) {
    origin = null;
    currentBlockRelative = null;
    ready = false;

    if (refillTimer != null) {
      refillTimer.cancel();
      refillTimer.purge();
      refillTimer = null;
    }
  }

  public static class Listener {
    public void ready () {}
  }

  public void addListener (Listener listener) { listeners.add(listener); }
}
