package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.Client;
import org.cloudburstmc.math.vector.Vector3i;
import com.github.steveice10.packetlib.packet.Packet;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.SessionListener;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.level.ServerboundBlockEntityTagQuery;
import com.github.steveice10.mc.protocol.packet.ingame.serverbound.level.ServerboundEntityTagQuery;
import com.github.steveice10.mc.protocol.packet.ingame.clientbound.level.ClientboundTagQueryPacket;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import java.util.concurrent.CompletableFuture;
import java.util.Map;
import java.util.HashMap;

public class QueryPlugin extends SessionAdapter {
  private Client client;
  private int nextTransactionId = 0;
  private Map<Integer, CompletableFuture<CompoundTag>> transactions = new HashMap<>();

  public QueryPlugin (Client client) {
    this.client = client;
    client.addListener((SessionListener) this);
  }

  public CompletableFuture<CompoundTag> block (Vector3i position) {
    final int transactionId = nextTransactionId++;
    if (nextTransactionId > Integer.MAX_VALUE) nextTransactionId = 0; // ? Can and should I use negative numbers too?
    client.session().send(new ServerboundBlockEntityTagQuery(transactionId, position));

    final CompletableFuture<CompoundTag> future = new CompletableFuture<CompoundTag>();
    transactions.put(transactionId, future);
    return future;
  }

  public CompletableFuture<CompoundTag> entity (int entityId) {
    final int transactionId = nextTransactionId++;
    if (nextTransactionId > Integer.MAX_VALUE) nextTransactionId = 0; // ? Can and should I use negative numbers too?
    client.session().send(new ServerboundEntityTagQuery(transactionId, entityId));

    final CompletableFuture<CompoundTag> future = new CompletableFuture<CompoundTag>();
    transactions.put(transactionId, future);
    return future;
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundTagQueryPacket) packetReceived(session, (ClientboundTagQueryPacket) packet);
  }

  public void packetReceived (Session session, ClientboundTagQueryPacket packet) {
    final CompletableFuture<CompoundTag> future = transactions.get(packet.getTransactionId());
    if (future == null) return;
    future.complete(packet.getNbt());
    transactions.remove(future);
  }
}
