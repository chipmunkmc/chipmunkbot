package land.chipmunk.chipmunkbot.systemChat;

import com.github.steveice10.mc.auth.data.GameProfile;
import com.github.steveice10.mc.protocol.data.game.entity.player.GameMode;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import land.chipmunk.chipmunkbot.data.chat.PlayerMessage;
import land.chipmunk.chipmunkbot.data.chat.SystemChatParser;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.TranslatableComponent;
import net.kyori.adventure.text.format.Style;

import java.util.List;
import java.util.UUID;

public class KaboomChatParser implements SystemChatParser {
  private final ChipmunkBot client;

  public KaboomChatParser (ChipmunkBot client) {
    this.client = client;
  }
  private static Component SEPERATOR_COLON = Component.text(":");
  private static Component SEPERATOR_COLON_RACCOON = Component.text("\u00a7f:"); // https://github.com/raccoonserver/extras/commit/315f704075db2f00e3e1bfbf55f858469c22880f
  private static Component SEPERATOR_SPACE = Component.space();

  @Override
  public PlayerMessage parse (Component message) {
    if (message instanceof TextComponent) return parse((TextComponent) message);
    return null;
  }

  public PlayerMessage parse (TextComponent message) {
    List<Component> children = message.children();

    if (!message.content().equals("") || !message.style().isEmpty() || children == null || children.size() < 3) return null;

    final Component prefix = children.get(0);
    Component displayName = Component.empty();
    Component contents = Component.empty();

    if (isSeperatorAt(children, 1)) { // Missing/blank display name
      if (children.size() > 3) contents = children.get(3);
    } else if (isSeperatorAt(children, 2)) {
      displayName = children.get(1);
      if (children.size() > 4) contents = children.get(4);
    } else {
      return null;
    }

    MutablePlayerListEntry sender = client.playerList().getEntry(Component.empty().append(prefix).append(displayName));

    return new PlayerMessage(sender, contents, "minecraft:chat", displayName);
  }

  private boolean isSeperatorAt (List<Component> children, int start) {
    return (children.get(start).equals(SEPERATOR_COLON) || children.get(start).equals(SEPERATOR_COLON_RACCOON)) && children.get(start + 1).equals(SEPERATOR_SPACE);
  }
}
