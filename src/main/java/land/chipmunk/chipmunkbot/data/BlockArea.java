package land.chipmunk.chipmunkbot.data;

import org.cloudburstmc.math.vector.Vector3i;
import lombok.Data;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
public class BlockArea {
  private Vector3i start;
  private Vector3i end;
}
