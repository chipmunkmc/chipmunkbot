package land.chipmunk.chipmunkbot.command;

import com.mojang.brigadier.Message;
import com.mojang.brigadier.exceptions.BuiltInExceptionProvider;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.CommandExceptionType;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.exceptions.Dynamic2CommandExceptionType; 
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentLike;
import net.kyori.adventure.text.TextComponent;

import java.util.Map;
import java.util.HashMap;

public class BuiltInExceptions implements BuiltInExceptionProvider {
  private static final Dynamic2CommandExceptionType DOUBLE_TOO_SMALL = new Dynamic2CommandExceptionType((found, min) -> message("argument.double.low", text(min), text(found)));
  private static final Dynamic2CommandExceptionType DOUBLE_TOO_BIG = new Dynamic2CommandExceptionType((found, max) -> message("argument.double.big", text(max), text(found)));

  private static final Dynamic2CommandExceptionType FLOAT_TOO_SMALL = new Dynamic2CommandExceptionType((found, min) -> message("argument.float.low", text(min), text(found)));
  private static final Dynamic2CommandExceptionType FLOAT_TOO_BIG = new Dynamic2CommandExceptionType((found, max) -> message("argument.float.big", text(max), text(found)));

  private static final Dynamic2CommandExceptionType INTEGER_TOO_SMALL = new Dynamic2CommandExceptionType((found, min) -> message("argument.integer.low", text(min), text(found)));
  private static final Dynamic2CommandExceptionType INTEGER_TOO_BIG = new Dynamic2CommandExceptionType((found, max) -> message("argument.integer.big", text(max), text(found)));

  private static final Dynamic2CommandExceptionType LONG_TOO_SMALL = new Dynamic2CommandExceptionType((found, min) -> message("argument.long.low", text(min), text(found)));
  private static final Dynamic2CommandExceptionType LONG_TOO_BIG = new Dynamic2CommandExceptionType((found, max) -> message("argument.long.big", text(max), text(found)));

  private static final DynamicCommandExceptionType LITERAL_INCORRECT = new DynamicCommandExceptionType(expected -> message("argument.literal.incorrect", text(expected)));

  private static final SimpleCommandExceptionType READER_EXPECTED_START_OF_QUOTE = new SimpleCommandExceptionType(message("parsing.quote.expected.start"));
  private static final SimpleCommandExceptionType READER_EXPECTED_END_OF_QUOTE = new SimpleCommandExceptionType(message("parsing.quote.expected.end"));
  private static final DynamicCommandExceptionType READER_INVALID_ESCAPE = new DynamicCommandExceptionType(character -> message("parsing.quote.escape", text(character)));
  private static final DynamicCommandExceptionType READER_INVALID_BOOL = new DynamicCommandExceptionType(value -> message("parsing.bool.invalid", text(value)));
  private static final DynamicCommandExceptionType READER_INVALID_INT = new DynamicCommandExceptionType(value -> message("parsing.int.invalid", text(value)));
  private static final SimpleCommandExceptionType READER_EXPECTED_INT = new SimpleCommandExceptionType(message("parsing.int.expected"));
  private static final DynamicCommandExceptionType READER_INVALID_LONG = new DynamicCommandExceptionType(value -> message("parsing.long.invalid", text(value)));
  private static final SimpleCommandExceptionType READER_EXPECTED_LONG = new SimpleCommandExceptionType(message("parsing.long.expected"));
  private static final DynamicCommandExceptionType READER_INVALID_DOUBLE = new DynamicCommandExceptionType(value -> message("parsing.double.invalid", text(value)));
  private static final SimpleCommandExceptionType READER_EXPECTED_DOUBLE = new SimpleCommandExceptionType(message("parsing.double.expected"));
  private static final DynamicCommandExceptionType READER_INVALID_FLOAT = new DynamicCommandExceptionType(value -> message("parsing.float.invalid", text(value)));
  private static final SimpleCommandExceptionType READER_EXPECTED_FLOAT = new SimpleCommandExceptionType(message("parsing.float.expected"));
  private static final SimpleCommandExceptionType READER_EXPECTED_BOOL = new SimpleCommandExceptionType(message("parsing.bool.expected"));
  private static final DynamicCommandExceptionType READER_EXPECTED_SYMBOL = new DynamicCommandExceptionType(symbol -> message("parsing.expected", text(symbol)));

  private static final SimpleCommandExceptionType DISPATCHER_UNKNOWN_COMMAND = new SimpleCommandExceptionType(message("command.unknown.command"));
  private static final SimpleCommandExceptionType DISPATCHER_UNKNOWN_ARGUMENT = new SimpleCommandExceptionType(message("command.unknown.argument"));
  private static final SimpleCommandExceptionType DISPATCHER_EXPECTED_ARGUMENT_SEPARATOR = new SimpleCommandExceptionType(message("command.expected.separator"));
  private static final DynamicCommandExceptionType DISPATCHER_PARSE_EXCEPTION = new DynamicCommandExceptionType(_message -> message("command.exception", text(_message)));

  @Override public Dynamic2CommandExceptionType doubleTooLow () { return DOUBLE_TOO_SMALL; }
  @Override public Dynamic2CommandExceptionType doubleTooHigh () { return DOUBLE_TOO_BIG; }
  @Override public Dynamic2CommandExceptionType floatTooLow () { return FLOAT_TOO_SMALL; }
  @Override public Dynamic2CommandExceptionType floatTooHigh () { return FLOAT_TOO_BIG; }
  @Override public Dynamic2CommandExceptionType integerTooLow () { return INTEGER_TOO_SMALL; }
  @Override public Dynamic2CommandExceptionType integerTooHigh () { return INTEGER_TOO_BIG; }
  @Override public Dynamic2CommandExceptionType longTooLow () { return LONG_TOO_SMALL; }
  @Override public Dynamic2CommandExceptionType longTooHigh () { return LONG_TOO_BIG; }
  @Override public DynamicCommandExceptionType literalIncorrect () { return LITERAL_INCORRECT; }
  @Override public SimpleCommandExceptionType readerExpectedStartOfQuote () { return READER_EXPECTED_START_OF_QUOTE; }
  @Override public SimpleCommandExceptionType readerExpectedEndOfQuote () { return READER_EXPECTED_END_OF_QUOTE; }
  @Override public DynamicCommandExceptionType readerInvalidEscape () { return READER_INVALID_ESCAPE; }
  @Override public DynamicCommandExceptionType readerInvalidBool () { return READER_INVALID_BOOL; }
  @Override public DynamicCommandExceptionType readerInvalidInt () { return READER_INVALID_INT; }
  @Override public SimpleCommandExceptionType readerExpectedInt () { return READER_EXPECTED_INT; }
  @Override public DynamicCommandExceptionType readerInvalidLong () { return READER_INVALID_LONG; }
  @Override public SimpleCommandExceptionType readerExpectedLong () { return READER_EXPECTED_LONG; }
  @Override public DynamicCommandExceptionType readerInvalidDouble () { return READER_INVALID_DOUBLE; }
  @Override public SimpleCommandExceptionType readerExpectedDouble () { return READER_EXPECTED_DOUBLE; }
  @Override public DynamicCommandExceptionType readerInvalidFloat () { return READER_INVALID_FLOAT; }
  @Override public SimpleCommandExceptionType readerExpectedFloat () { return READER_EXPECTED_FLOAT; }
  @Override public SimpleCommandExceptionType readerExpectedBool () { return READER_EXPECTED_BOOL; }
  @Override public DynamicCommandExceptionType readerExpectedSymbol () { return READER_EXPECTED_SYMBOL; }
  @Override public SimpleCommandExceptionType dispatcherUnknownCommand () { return DISPATCHER_UNKNOWN_COMMAND; }
  @Override public SimpleCommandExceptionType dispatcherUnknownArgument () { return DISPATCHER_UNKNOWN_ARGUMENT; }
  @Override public SimpleCommandExceptionType dispatcherExpectedArgumentSeparator () { return DISPATCHER_EXPECTED_ARGUMENT_SEPARATOR; }
  @Override public DynamicCommandExceptionType dispatcherParseException () { return DISPATCHER_PARSE_EXCEPTION; }

  private static Message message (final String key, final ComponentLike... args) { return ComponentMessage.wrap(Component.translatable(key, args)); } 
  private static TextComponent text (final Object text) { return Component.text(String.valueOf(text)); }
}
