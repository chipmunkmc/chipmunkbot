package land.chipmunk.chipmunkbot;

import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.packetlib.ProxyInfo;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.tcp.TcpClientSession;
import com.github.steveice10.packetlib.packet.Packet;
import com.github.steveice10.packetlib.event.session.*;
import com.github.steveice10.mc.auth.data.GameProfile;
import com.github.steveice10.mc.protocol.packet.login.clientbound.ClientboundGameProfilePacket;
import java.util.Timer;
import java.util.TimerTask;
import java.util.List;
import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;

public class Client {
  @Getter private Session session;
  @Getter private final List<SessionListener> listeners = new ArrayList<>();
  @Getter private GameProfile profile;

  private String host;
  private int port;
  private MinecraftProtocol protocol;
  private ProxyInfo proxy;

  @Getter @Setter private long reconnectDelay;
  @Getter @Setter private List<Client> allClients;

  public Client (String host, int port, MinecraftProtocol protocol, ProxyInfo proxy, long reconnectDelay, List<Client> allClients) {
    this.host = host;
    this.port = port;
    this.protocol = protocol;
    this.proxy = proxy;
    this.reconnectDelay = reconnectDelay;
    this.allClients = allClients;
    reconnect();
  }

  public void addListener (SessionListener listener) { listeners.add(listener); }

  public void reconnect () { // ? Should this be public?
    final Session session = new TcpClientSession(host, port, protocol, proxy);
    this.session = session;

    session.addListener(new SessionAdapter () {
      @Override public void packetReceived (Session session, Packet packet) {
        if (packet instanceof ClientboundGameProfilePacket) profile = ((ClientboundGameProfilePacket) packet).getProfile();
        for (SessionListener listener : listeners) listener.packetReceived(session, packet);
      }
      @Override public void packetSending (PacketSendingEvent event) { for (SessionListener listener : listeners) listener.packetSending(event); }
      @Override public void packetSent (Session session, Packet packet) { for (SessionListener listener : listeners) listener.packetSent(session, packet); }
      @Override public void packetError (PacketErrorEvent event) {
        event.getCause().printStackTrace();
        event.setSuppress(true);
        for (SessionListener listener : listeners) listener.packetError(event);
      }
      @Override public void connected (ConnectedEvent event) { for (SessionListener listener : listeners) listener.connected(event); }
      @Override public void disconnecting (DisconnectingEvent event) { for (SessionListener listener : listeners) listener.disconnecting(event); }

      @Override
      public void disconnected (DisconnectedEvent event) {
        for (SessionListener listener : listeners) listener.disconnected(event);

        if (reconnectDelay < 0) return;

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
          @Override
          public void run () {
            reconnect();

            timer.purge();
          }
        };

        timer.schedule(task, reconnectDelay);
      }
    });

    session.connect();
  }
}
