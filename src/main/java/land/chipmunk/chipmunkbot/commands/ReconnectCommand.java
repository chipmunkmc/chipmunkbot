package land.chipmunk.chipmunkbot.commands;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.kyori.adventure.text.Component;

public class ReconnectCommand {
  public static void register (CommandDispatcher dispatcher) {
    final ReconnectCommand instance = new ReconnectCommand();

    dispatcher.register(
      literal("reconnect")
        .executes(instance::reconnect)
    );
  }

  public int reconnect (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();

    source.sendOutput(Component.translatable("Reconnecting!"));
    client.session().disconnect("quit");

    return 1;
  }
}
