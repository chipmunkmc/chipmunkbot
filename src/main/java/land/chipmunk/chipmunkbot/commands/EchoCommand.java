package land.chipmunk.chipmunkbot.commands;

import land.chipmunk.chipmunkbot.ChipmunkBot; 
import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.kyori.adventure.text.Component;

public class EchoCommand {
  public static void register (CommandDispatcher dispatcher) {
    final EchoCommand instance = new EchoCommand();

    dispatcher.register(
      literal("echo")
        .then(
          argument("text", greedyString())
            .executes(instance::echo)
        )
    );
  }

  public int echo (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    final String text = getString(context, "text");

    source.sendOutput(Component.text(text));

    return 1;
  }
}