package land.chipmunk.chipmunkbot.commands;

import land.chipmunk.chipmunkbot.Client;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.mc.protocol.data.ProtocolState;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

public class NetMsgCommand {
  public static void register (CommandDispatcher dispatcher) {
    final NetMsgCommand instance = new NetMsgCommand();

    dispatcher.register(
      literal("netmsg")
        .then(
          argument("message", greedyString())
            .executes(instance::netmsg)
        )
    );
  }

  public int netmsg (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();
    final String input = getString(context, "message");

    final Component message = Component.translatable(
      "[%s] %s \u203a %s",
      Component.text(client.session().getHost() + ":" + client.session().getPort(), NamedTextColor.GRAY),
      Component.empty().color(NamedTextColor.DARK_GREEN).append(source.displayName()),
      Component.text(input, NamedTextColor.GRAY)
    ).color(NamedTextColor.DARK_GRAY);

    for (Client remote : source.client().allClients()) {
      if (!(remote instanceof ChipmunkBot)) continue; // ? Is this optimal?

      final Session session = remote.session();
      final MinecraftProtocol protocol = (MinecraftProtocol) session.getPacketProtocol();
      if (!session.isConnected() || protocol.getState() != ProtocolState.GAME) continue;

      ((ChipmunkBot) remote).chat().tellraw(message);
    }

    return 1;
  }
}